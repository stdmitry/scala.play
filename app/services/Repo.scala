package services

import models._
import javax.inject._

trait Repo {
  def fetchAll() : List[Employee]
  def findByName(name : String) : List[Employee]
  def findByDepartment(dep : String) : List[Employee]
  def add(e : Employee)
}

@Singleton
class LocalRepo extends Repo {
  var data = Array(
    new Employee("Leroy Jenkins", "dev", 100),
    new Employee("Duke Nukem", "dev", 115),
    new Employee("Gordon Freeman", "qa", 90),
    new Employee("Max Pain", "qa", 130),
    new Employee("Jim Reynor", "hq", 130),
    new Employee("Sara Kerrigan", "hr", 130)
  )

  override def fetchAll() = this.data.toList
  override def findByName(name : String) = this.data.filter(_.name.toLowerCase().contains(name)).toList
  override def findByDepartment(dep : String) = this.data.filter(_.dep == dep).toList
  override def add(e: Employee) = this.data = this.data :+ e
}
