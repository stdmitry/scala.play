package controllers

import javax.inject._
import play.api.mvc._
import services.Repo
import models._
import play.api.data._
import play.api.data.Forms._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject() (repo : Repo) extends Controller {

  def index = Action {
    Ok(views.html.index2(repo.fetchAll()))
  }

  def find (name : Option[String]) = Action {
    Ok(views.html.index2(repo.findByName(name.getOrElse(""))))
  }

  def add = Action { implicit request =>
    val addForm = Form(
      mapping(
        "name" -> text,
        "dep" -> text,
        "salary" -> number
      )(EmployeeData.apply)(EmployeeData.unapply)
    )

    addForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.error("form error"))
      },
      employeeData => {
        repo.add(new Employee(employeeData.name, employeeData.dep, employeeData.salary))
        Redirect("/")
      }
    )
  }

  def salary(dep : Option[String]) = Action {
    val staff =
      dep match {
        case Some(department) => repo.findByDepartment(department)
        case None => repo.fetchAll()
      }

    Ok(views.html.salary(dep, staff.foldLeft(0)(_ + _.salary)))
  }

  def avg(dep : Option[String]) = Action {
    val staff =
      dep match {
        case Some(department) => repo.findByDepartment(department)
        case None => repo.fetchAll()
      }

    val salaryList = staff.map(_.salary)

    Ok(views.html.avg(dep, salaryList.sum / salaryList.length))
  }
}
