package models

/**
  * Created by pmpc010 on 4/26/16.
  */

class Employee (val name: String, val dep: String, val salary: Int) {
  override def toString = s"[${this.dep}]${this.name}\t\t${this.salary}"
}

case class EmployeeData(name: String, dep: String, salary: Int)